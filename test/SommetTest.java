import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class SommetTest {

    private Sommet sommet;

    @Before
    public void setUp() {
        // Initialisation d'un sommet avec un numéro donné
        sommet = new Sommet(1);
    }

    @Test
    public void testGetNumero() {
        // Vérification que le numéro est bien celui passé lors de l'initialisation
        assertEquals(1, sommet.getNumero());
    }

    @Test
    public void testInitialParentIsSelf() {
        // Vérification que le parent initial du sommet est lui-même
        assertEquals(sommet, sommet.getParent());
    }

    @Test
    public void testSetParent() {
        // Création d'un nouveau sommet pour tester la fonction setParent
        Sommet sommetParent = new Sommet(2);
        sommet.setParent(sommetParent);

        // Vérification que le parent a bien été mis à jour
        assertEquals(sommetParent, sommet.getParent());
    }

    @Test
    public void testGetAndSetRang() {
        // Le rang initial doit être 0
        assertEquals(0, sommet.getRang());

        // Mise à jour du rang
        sommet.setRang(5);

        // Vérification que le rang est bien celui défini
        assertEquals(5, sommet.getRang());
    }

    @Test
    public void testToString() {
        // Vérification du format du toString
        assertEquals("Sommet 1", sommet.toString());
    }
}
