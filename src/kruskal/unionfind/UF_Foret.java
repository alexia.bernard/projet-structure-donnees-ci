package kruskal.unionfind;

import kruskal.Sommet;

public class UF_Foret implements UnionFind{

    public UF_Foret(){}

    public void makeSet(Sommet x){
        x.setParent(x);
        x.setRang(0);
    }


    public void union(Sommet x,Sommet y)
    {  
        Sommet representantX = find(x);
        Sommet representantY = find(y);
        if(representantX == representantY){
            throw new IllegalArgumentException("Les ensembles contenant x et y ne sont pas disjoints");
        }
        link(representantX, representantY);
    }

    private void link(Sommet x, Sommet y){
        int rangX = x.getRang();
        int rangY = y.getRang();
        if(rangX > rangY){
            y.setParent(x);
        }else{
            x.setParent(y);
        }
        if(rangX == rangY){
            y.setRang(rangY+1);
        }
    }

    public Sommet find(Sommet x){
        Sommet parent = x.getParent();
        if(parent != x){
            x.setParent(find(parent));
        }
        return x.getParent();
    }
}
