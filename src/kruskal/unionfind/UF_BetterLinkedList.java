package kruskal.unionfind;
import java.util.ArrayList;
import java.util.HashMap;

import kruskal.BetterLinkedList;
import kruskal.Sommet;

public class UF_BetterLinkedList implements UnionFind
{

    /**
     * La liste des représentants des différents ensembles
     */
    private ArrayList<Sommet> representants;
    private HashMap<Sommet, BetterLinkedList<Sommet>> representing;


    public UF_BetterLinkedList()
    {
        this.representants = new ArrayList<Sommet>();
        this.representing = new HashMap<Sommet, BetterLinkedList<Sommet>>();
    }

    public void makeSet(Sommet x)
    {
        if(find(x) != null){
            throw new IllegalArgumentException("Le sommet numéro "+x.getNumero()+" est déjà membre d'un autre ensemble");
        }

        BetterLinkedList<Sommet> newEnsemble = new BetterLinkedList<>();
        newEnsemble.add(x);
        this.representants.add(x);
        this.representing.put(x, newEnsemble);
    }

    public void union(Sommet x,Sommet y)
    {
        Sommet representantX = find(x);
        BetterLinkedList<Sommet> ensembleX = representing.get(representantX);

        Sommet representantY = find(y);
        BetterLinkedList<Sommet> ensembleY = representing.get(representantY);

        if(!areDisjoints(ensembleX, ensembleY)){
            throw new IllegalArgumentException("Les ensembles contenant x et y ne sont pas disjoints");
        }

        // On ajoute les éléments de l'ensemble le plus petit à l'ensemble le plus grand
        if(ensembleX.size() < ensembleY.size()){
            this.updateRepresentants(representantX, representantY);
        } else {
            this.updateRepresentants(representantY, representantX);
        }
        
    }

    private void updateRepresentants(Sommet oldR,  Sommet newR){
        BetterLinkedList<Sommet> representedByOld = this.representing.get(oldR);
        BetterLinkedList<Sommet> representedByNew = this.representing.get(newR);

        representedByNew.addAll(representedByOld);

        this.representing.put(newR, representedByNew);
        this.representing.remove(oldR);

        int idOldR = oldR.getNumero()-1;
        this.representants.set(idOldR, newR);
        for(Sommet s : representedByOld){
            int idS = s.getNumero()-1;
            this.representants.set(idS, newR);
        }
    }

    public boolean areDisjoints(BetterLinkedList<Sommet> ensembleX, BetterLinkedList<Sommet> ensembleY)
    {
        for(Sommet sommet : ensembleX){
            if(ensembleY.contains(sommet)){
                return false;
            }
        }
        return true;
    }

    public Sommet find(Sommet x)
    {
        if(x.getNumero() > this.representants.size()){
            return null;
        }
        return this.representants.get(x.getNumero()-1);
    }
    
}
