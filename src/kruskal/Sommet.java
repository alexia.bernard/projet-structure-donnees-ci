package kruskal;

/**
 * Classe qui gère les sommets
 * 
 * @author Aléxia Bernard & Maxime Cardinale
 */
public class Sommet {

    /**
     * Le numéro du sommet
     */
    private int numero;
    private int rang;
    private Sommet parent;

    /**
     * @param numero - le numéro du sommet
     */
    public Sommet(int numero){
        this.numero = numero;
        this.rang = 0;
        this.parent = this;
    }

    public void setParent(Sommet parent){
        this.parent = parent;
    }

    public Sommet getParent(){
        return this.parent;
    }

    public void setRang(int rang){
        this.rang = rang;
    }

    public int getRang(){
        return this.rang;
    }

    /**
     * Renvoie le numéro du sommet
     *
     * @return le numéro
     */
    public int getNumero(){ 
        return this.numero; 
    }

    @Override
    public String toString(){
        return "Sommet " + this.numero;
    }
}
