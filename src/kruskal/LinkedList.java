package kruskal;
import java.util.Iterator;

public class LinkedList<E> implements Iterable<E>{

    private Node<E> head;

    public LinkedList() {
        this.head = null;
    }

    public void add(E element) {
        Node<E> newNode = new Node<E>(element);
        newNode.setNext(this.head);
        this.head = newNode;
    }

    public boolean contains(E element) {
        for(E current : this) {
            if (current.equals(element)) {
                return true;
            }
        }
        return false;
    }

    public void addAll(LinkedList<E> other) {
        for(E element : other) {
            this.add(element);
        }
    }

    public void remove(E element) {
        if (this.head == null) {
            return;
        }
        if (this.head.getValue().equals(element)) {
            this.head = this.head.getNext();
            return;
        }
        Node<E> current = this.head;
        while (current.getNext() != null) {
            if (current.getNext().getValue().equals(element)) {
                current.setNext(current.getNext().getNext());
                return;
            }
            current = current.getNext();
        }
    }

    public E getHead() {
        return this.head.getValue();
    }

    // ---------------------------- Implements Iterable<E> interface ---------------------------- //

    public Iterator<E> iterator() {
        return new LinkedListIterator<E>(this.head);
    }

    private class LinkedListIterator<F> implements Iterator<F> {

        private Node<F> current;

        public LinkedListIterator(Node<F> head) {
            this.current = head;
        }

        public boolean hasNext() {
            return this.current != null;
        }

        public F next() {
            F element = this.current.getValue();
            this.current = this.current.getNext();
            return element;
        }
    }
}
