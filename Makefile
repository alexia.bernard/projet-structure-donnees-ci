SRCPATH = src/kruskal
TESTPATH = test
CHECKSTYLE_JAR = checkstyle-8.42-all.jar
CHECKSTYLE_CONFIG = checkstyle.xml
JUNIT_JAR = junit-4.10.jar
JACOCO_A = org.jacoco.agent-0.8.12.202403310830.jar
JACOCO_R = org.jacoco.report-0.8.12.202403310830.jar


default : compile

compile :
	javac $(SRCPATH)/*.java $(SRCPATH)/unionfind/*.java $(TESTPATH)/*.java

run :
	java -javaagent:$(JACOCO_A)=destfile=jacoco.exec -cp src kruskal.Main $(args)

# Génération du rapport JaCoCo
report:
	java -cp $(JACOCO_R):$(SRCPATH) org.jacoco.report.ReportGenerator

# Exécution des tests
test:
	java -jar $(JUNIT_JAR) --class-path $(TESTPATH) --scan-class-path

# Cible pour exécuter Checkstyle
checkstyle: 
    java -jar $(CHECKSTYLE_JAR) -c $(CHECKSTYLE_CONFIG) $(SRCPATH)

all: clean compile run

clean :
	rm -f $(SRCPATH)/*.class $(SRCPATH)/unionfind/*.class 
